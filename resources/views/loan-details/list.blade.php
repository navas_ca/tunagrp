<div class="table-responsive">
	<table class="table table-bordered">
		<thead>
			<tr>
				<th>Client id</th>
				@foreach ($months as $month => $val)
					<th>{{ $month }}</th>
				@endforeach
			</tr>
		</thead>
		<tbody>
			@foreach ($emiRecords as $key => $record)
				<tr>
					<td>{{ $record['clientid'] }}</td>
					@foreach ($months as $month => $val)
						<td>{{ $record[$month] }}</td>
					@endforeach
				</tr>
			@endforeach
		</tbody>
	</table>
</div>