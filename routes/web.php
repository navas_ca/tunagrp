<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers as CTRL;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes(['register'=>false]);

Route::get('/home', [CTRL\HomeController::class, 'index'])->name('home');

Route::group(['middleware' => 'auth'], function ($request) {
	Route::get('loan-details', [CTRL\LoanDetailsController::class, 'index'])->name('loan.details.index');
	Route::get('loan-details/load', [CTRL\LoanDetailsController::class, 'load'])->name('loan.details.load');
});
