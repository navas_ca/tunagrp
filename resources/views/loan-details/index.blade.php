@extends('layouts.app')

@section('content')
<div class="container">    
    <div class="row">
        <div class="col col-xs-12">
            <button class="process-data btn btn-primary">Process data</button>
        </div>        
    </div>

    <div class="listHtml"></div>

    <div class="success-response"></div>
    <div class="error-response"></div>
</div>
@endsection


@section('scripts')
    <script type="text/javascript">        
        $(document).ready(function() {
            $('.process-data').click(function() {
                $('.process-data').attr('disabled', true)
                $('.listHtml').html('')
                $.get({
                    url: '{{ route('loan.details.load') }}',
                    success: function( data, textStatus, jQxhr ){
                        $('.listHtml').html(data.data.listHtml)
                        $('.error-response').html('')
                    },
                    error: function( jqXhr, textStatus, errorThrown ){
                        $('.error-response').html('')
                    },
                    complete: function() {
                       $('.process-data').attr('disabled', false)
                    }
                });
            })
        })
        
    </script>
@endsection
