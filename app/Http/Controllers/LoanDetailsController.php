<?php

namespace App\Http\Controllers;

use App\Models\LoanDetails;
use Illuminate\Http\Request;
use DB;
use \Exception;
use Carbon\Carbon;
class LoanDetailsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('loan-details.index');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function load()
    {
        try {
            DB::beginTransaction();
            DB::statement('DROP TABLE IF EXISTS emi_details;');
            $res = LoanDetails::select(
                        DB::raw('min(first_payment_date) as first_payment_date'),
                        DB::raw('max(last_payment_date) as last_payment_date')
                        )->first();

            $emiRecords = $months = [];
            $from = new Carbon($res['first_payment_date']);
            $to = new Carbon($res['last_payment_date']);            
            $cols = [];
            while ($from->format('Ym') <= $to->format('Ym')) {
                $cols[] = "`". $from->format('Y_M'). "` double default 0";
                $months[$from->format('Y_M')] = 0;
                $from = $from->addMonth(1);
            }
            $sql = "
                CREATE TABLE `emi_details` (
                 `clientid` int unsigned NOT NULL AUTO_INCREMENT, ".
                 implode(",", $cols) . ",
                 PRIMARY KEY (`clientid`)
                )
            ";
            DB::statement($sql);
            $loanRecords = LoanDetails::all();
            
            foreach ($loanRecords as $loan) {
                $record = $months;
                $record['clientid'] = $loan->clientid;
                $emi_amount = $this->round($loan->loan_amount / $loan->num_of_payment);
                $from = new Carbon($loan->first_payment_date);
                $to = new Carbon($loan->last_payment_date);
                $i = 0;
                $sum = 0;
                while ($from->format('Ym') <= $to->format('Ym')) {
                    if ($i >= $loan->num_of_payment) {
                        break;
                    }
                    if ($this->round($sum + $emi_amount) > $loan->loan_amount) {
                        $emi_amount = $this->round($emi_amount - ($sum + $emi_amount - $loan->loan_amount));
                    }            
                    if ($i+1 >= $loan->num_of_payment && $this->round($sum + $emi_amount) < $loan->loan_amount) {
                        $emi_amount = $this->round($emi_amount + ($loan->loan_amount - ($sum + $emi_amount)));
                    }
                    $sum = $this->round($sum + $emi_amount);
                    $record[$from->format('Y_M')] = $emi_amount;
                    $from = $from->addMonthsNoOverflow();
                    $i++;
                }
                $emiRecords[] = $record;
            }
            DB::table('emi_details')->insert($emiRecords);
            DB::commit();
            $rendered = view('loan-details.list', compact('emiRecords', 'months'))->render();
            return response()->json(['data' => ['listHtml' => $rendered]]);

        } catch (Exception $e) {
            DB::rollback();
            return response()->json(['message' => $e->getMessage()], 400);
        }

        $loanDetails = LoanDetails::orderBy('clientid', 'desc')->get();
        return $rendered = view('loan-details.list', compact('loanDetails'))->render();
        return $rendered;
    }

    public function round($value)
    {
        return round($value, 2);
    }
}
